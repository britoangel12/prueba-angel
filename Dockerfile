FROM node:alpine
RUN addgroup -g 2021 -S devops
RUN adduser -u 2022 -S abrito -g devops
RUN mkdir -p /usr/src/app && chown -R abrito:devops /usr/src/app
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN apk --no-cache add curl
RUN npm install
COPY . /usr/src/app
USER abrito
EXPOSE 3000
CMD [ "npm", "start" ]